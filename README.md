## ncun-cli
诺春博客主题开发脚手架

## Installation

``` bash
$ npm install ncun-cli -g  
```

## template
主题开发模板支持框架有 vue, react, angular

### vue
[https://github.com/dianjiu/ncun-cli-vue-demo.git](https://github.com/dianjiu/ncun-cli-vue-demo.git)  
[https://gitee.com/dianjiu/ncun-cli-vue-demo.git](https://gitee.com/dianjiu/ncun-cli-vue-demo.git)

### react
[https://github.com/dianjiu/ncun-cli-react-demo.git](https://github.com/dianjiu/ncun-cli-react-demo.git)  
[https://gitee.com/dianjiu/ncun-cli-react-demo.git](https://gitee.com/dianjiu/ncun-cli-react-demo.git)

### angular
[https://github.com/dianjiu/ncun-cli-angular-demo.git](https://github.com/dianjiu/ncun-cli-angular-demo.git)  
[https://gitee.com/dianjiu/ncun-cli-angular-demo.git](https://gitee.com/dianjiu/ncun-cli-angular-demo.git)

## License

[MIT](LICENSE)
