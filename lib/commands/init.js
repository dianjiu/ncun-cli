const { prompt } = require('inquirer');
const program = require('commander');
const shell = require('shelljs');
const ora = require('ora');
const fs = require('fs');
const config = require('../../templates.json')
const chalk = require('chalk')

const option =  program.parse(process.argv).args[0];
const question = [
  {
    type: 'input',
    name: 'template',
    message: 'Template name (you can input one like react, vue, angular):',
    default: typeof option === 'string' ? option : 'ncun-blog-template',
    filter (val) {
      return val.trim()
    },
    validate (val) {
      /* const validate = (val.trim().split(" ")).length === 1;
      return validate || 'Project name is not allowed to have spaces '; */
      const validate = config.tpl[val];
      if(validate){
        return true
      }
      return chalk.red('Template does not support!');
    },
    transformer (val) {
      return val;
    }
  },
  {
    type: 'input',
    name: 'name',
    message: 'Project name',
    default: typeof option === 'string' ? option : 'ncun-blog-template',
    filter (val) {
      return val.trim()
    },
    validate (val) {
      const validate = (val.trim().split(" ")).length === 1;
      return validate || 'Project name is not allowed to have spaces ';
    },
    transformer (val) {
      return val;
    }
  },
  {
    type: 'input',
    name: 'description',
    message: 'Project description',
    default: 'Vue project',
    validate () {
      return true;
    },
    transformer(val) {
      return val;
    }
  },
  {
    type: 'input',
    name: 'author',
    message: 'Author',
    default: '',
    validate () {
      return true;
    },
    transformer(val) {
      return val;
    }
  }
];

module.exports = prompt(question).then(({template, name, description, author}) => {
  gitUrl = config.tpl[template].url
  branch = config.tpl[template].branch
  const projectName = name;
  const spinner = ora('Downloading please wait... \n');

  spinner.start();
  //克隆模板
  let cmdStr = `git clone -b ${branch} ${gitUrl} ${projectName} `
  // childProcess.exec(cmdStr, function(error, stdout, stderr){
  if(shell.exec(cmdStr).code !== 0){
    shell.echo('Error: Git clone failed');
    shell.exit(1);
  }
  
  
  //读取package.json
  fs.readFile(`./${projectName}/package.json`, 'utf8', function (err, data) {
    if(err) {
      spinner.stop();
      console.error(err);
      return;
    }

    const packageJson = JSON.parse(data);
    packageJson.name = name;
    packageJson.description = description;
    packageJson.author = author;
    
    //修改package.json
    fs.writeFile(`./${projectName}/package.json`, JSON.stringify(packageJson, null, 2), 'utf8', function (err) {
      if(err) {
        spinner.stop();
        console.error(err);
      } else {
        spinner.stop();
        console.log(chalk.green('NCun init successfully. Now run:'))
        console.log(`
          ${chalk.yellow(`cd ${projectName}`)}
          ${chalk.yellow('npm install (or `yarn`)')}
          ${chalk.yellow('npm run dev (or `yarn dev`)')}
        `);
      }
    });
  });

  console.log(chalk.green('\n √ Generation completed!'))
});
